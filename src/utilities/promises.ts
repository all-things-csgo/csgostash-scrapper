export const sleep = (ms: number): Promise<void> =>
  new Promise(resolve => (ms > 0 ? setTimeout(resolve, ms) : resolve()));

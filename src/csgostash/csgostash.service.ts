import fs from 'fs';
import axios, { AxiosInstance } from 'axios';

import {
  CSGOStashSkin,
  CSGOStashCategories,
  CSGOStashCategory,
  CSGOStashCategorySkins,
  CSGOStashCategorySkin,
  CSGOStashCategoryType,
} from './csgostash.types';
import {
  parseCSGOStashSkin,
  parseCSGOStashCategories,
  parseCSGOStashCategorySkins,
} from './csgostash.parser';
import { randomNumber } from '../utilities';

export default class CSGOStashService {
  public static readonly BASE_URL = 'https://csgostash.com';
  public static readonly ROOT_URL = '/';
  public static readonly SKIN_URL = 'skin';

  private readonly http: AxiosInstance;

  constructor() {
    this.http = axios.create({
      baseURL: CSGOStashService.BASE_URL,
      timeout: 10000,
      headers: {
        'Content-Type': 'text/xml',
      },
    });
  }

  public async fetchCategories(
    categoryTypes?: CSGOStashCategoryType[],
  ): Promise<CSGOStashCategories> {
    try {
      const config = {
        headers: { 'user-agent': this.randomUserAgent() },
      };
      const result = await this.http.get<string>(`${CSGOStashService.ROOT_URL}`, config);

      const parsedResult = parseCSGOStashCategories(result.data, categoryTypes);
      if (!parsedResult) {
        return Promise.reject(new Error(`Failed to parse categories`));
      }

      return parsedResult;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  public async fetchCategorySkins(category: CSGOStashCategory): Promise<CSGOStashCategorySkins> {
    try {
      if (!category || !category.name || !category.url) {
        return Promise.reject(new Error('Invalid category'));
      }

      const categoryUrl = this.extractPartialUrl(category.url);
      if (!categoryUrl) {
        return Promise.reject(new Error('Invalid category url'));
      }

      const config = {
        headers: { 'user-agent': this.randomUserAgent() },
      };
      const result = await this.http.get<string>(categoryUrl, config);

      const parsedResult = parseCSGOStashCategorySkins(result.data);
      if (!parsedResult) {
        return Promise.reject(
          new Error(`Failed to parse category skins "${category.name}" - "${category.url}`),
        );
      }

      return parsedResult;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  public async fetchCategorySkin(categorySkin: CSGOStashCategorySkin): Promise<CSGOStashSkin> {
    try {
      if (!categorySkin || !categorySkin.name || !categorySkin.url) {
        return Promise.reject(new Error('Invalid category skin'));
      }

      const categoryUrl = this.extractPartialUrl(categorySkin.url);
      if (!categoryUrl) {
        return Promise.reject(new Error('Invalid category skin url'));
      }

      const config = {
        headers: { 'user-agent': this.randomUserAgent() },
      };
      const result = await this.http.get<string>(categoryUrl, config);

      const parsedResult = parseCSGOStashSkin(result.data);
      if (!parsedResult) {
        return Promise.reject(
          new Error(`Failed to parse skin "${categorySkin.name}" - "${categorySkin.url}"`),
        );
      }

      return parsedResult;
    } catch (e) {
      return Promise.reject(e);
    }
  }
  public async fetchAndSaveSkin(sourceUrl: string, targetPath: string): Promise<void> {
    if (!sourceUrl || !targetPath) {
      return Promise.reject(new Error('invalid parameters'));
    }

    try {
      if (fs.existsSync(targetPath)) {
        console.log(`Skin already downloaded: "${targetPath}"`);
        return Promise.resolve();
      }

      const writer = fs.createWriteStream(targetPath);

      const response = await axios({
        url: sourceUrl,
        method: 'GET',
        responseType: 'stream',
      });

      response.data.pipe(writer);

      return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
      });
    } catch (e) {
      return Promise.reject(e);
    }
  }

  private randomUserAgent(): string {
    const userAgents = [
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:53.0) Gecko/20100101 Firefox/53.0',
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393',
      'Mozilla/5.0 (iPad; CPU OS 8_4_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12H321 Safari/600.1.4',
      'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1',
      'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-G570Y Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36',
      'Mozilla/5.0 (Linux; Android 5.0; SAMSUNG SM-N900 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/2.1 Chrome/34.0.1847.76 Mobile Safari/537.36',
      'Mozilla/5.0 (Linux; Android 6.0.1; SAMSUNG SM-N910F Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/4.0 Chrome/44.0.2403.133 Mobile Safari/537.36',
      'Mozilla/5.0 (Linux; U; Android-4.0.3; en-us; Galaxy Nexus Build/IML74K) AppleWebKit/535.7 (KHTML, like Gecko) CrMo/16.0.912.75 Mobile Safari/535.7',
      'Mozilla/5.0 (Linux; Android 7.0; HTC 10 Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Mobile Safari/537.36',
    ];

    const idx = randomNumber(0, userAgents.length - 1);
    return userAgents[idx];
  }

  private extractPartialUrl(url: string): string | null {
    if (!url) {
      return null;
    }

    const idx = url.indexOf(CSGOStashService.BASE_URL);
    if (idx === -1) {
      return null;
    }

    return url.substring(CSGOStashService.BASE_URL.length);
  }
}

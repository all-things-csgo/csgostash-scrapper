import cheerio from 'cheerio';

import {
  CSGOStashWeaponNamePrefix,
  CSGOStashWeaponNamePostfix,
  CSGOStashWeaponNameHashDelimiter,
  CSGOStashWeaponNameDelimiter,
  CSGOStashWeapons,
  CSGOStashSkin,
  CSGOStashCategories,
  CSGOStashCategorySkins,
  CSGOStashCategoryTypeSet,
  CSGOStashCategoryType,
} from './csgostash.types';

export const parseCategories = (
  $: CheerioStatic,
  categoryTypes?: CSGOStashCategoryType[],
): CSGOStashCategories | null => {
  const $navbar = $('ul.navbar-nav li.dropdown');

  if (!$navbar) {
    return null;
  }

  const categoryTypeSet = categoryTypes
    ? new Set(categoryTypes.map(type => CSGOStashCategoryType[type]))
    : CSGOStashCategoryTypeSet;
  const result: CSGOStashCategories = [];

  $navbar.each((_: number, element: any) => {
    const $element = $(element);
    const $title = $('a.dropdown-toggle', $element);
    const $links = $('ul > li > a', $element);

    const category = $title.text();
    if (!category || !categoryTypeSet.has(category)) {
      return true;
    }

    $links.each((_: number, link: any) => {
      const $link = $(link);
      const url = $link.attr('href');
      const name = $link.text();

      // NOTE(jsh): Check if this is a weapon link, if not just ignore it
      if (!name || !url || url.indexOf(CSGOStashWeaponNamePrefix) === -1) {
        return false;
      }

      result.push({
        category: CSGOStashCategoryType[category as keyof typeof CSGOStashCategoryType],
        name,
        url,
      });
      return true;
    });

    return true;
  });

  return result.length > 0 ? result : null;
};

export const parseCategorySkins = ($: CheerioStatic): CSGOStashCategorySkins | null => {
  const $container = $('div.main-content div.row div[class~="result-box"]');

  if (!$container) {
    return null;
  }

  const result: CSGOStashCategorySkins = [];

  $container.each((_: number, element: any) => {
    const $element = $(element);
    const $name = $('h3 > a', $element);

    const name = $name.text();
    if (!name) {
      // NOTE(jsh): Probably an "Ad" that was blocked... ignore this container and continue anyways
      return true;
    }

    const $link = $('a:nth-of-type(2)', $element);
    const url = $link.attr('href');
    if (!url) {
      // NOTE(jsh): Probably an "Ad" that was blocked... ignore this container and continue anyways
      return true;
    }

    result.push({
      name,
      url,
    });

    return true;
  });

  return result.length > 0 ? result : null;
};

export const parseOGURL = ($: CheerioStatic): string | null => {
  const ogImage = $('head meta[property="og:url"]');
  return ogImage.attr('content') || null;
};

export const parseOG3dImageURL = ($: CheerioStatic): string | null => {
  const image = $('head meta[property="og:image"]');
  return image.attr('content') || null;
};

export const parseSideImageURL = ($: CheerioStatic): string | null => {
  const image = $('body div#preview-default a > img');
  return image.attr('src') || null;
};

// NOTE(jsh): Poor mans parser...
// EXAMPLE URL: https://steamcdn-a.akamaihd.net/apps/730/icons/econ/default_generated/weapon_ssg08_cu_ssg08_deathshead_light_large.78676bc5fce74af519abdccae4d119727c392faf.png
export const parseWeaponAndSkinName = (imageUrl: string): [string, string] | [null, null] => {
  if (typeof imageUrl !== 'string' || !imageUrl.length) {
    return [null, null];
  }

  const removeWeaponPrefixAndExtractFullWeaponAndSkinName = (
    value: string | null,
  ): string | null => {
    if (!value) {
      return null;
    }

    const weaponWithDelimiter = `${CSGOStashWeaponNamePrefix}${CSGOStashWeaponNameDelimiter}`;
    const idx = value.indexOf(weaponWithDelimiter);
    if (idx === -1) {
      return null;
    }

    const result = value.substring(idx + weaponWithDelimiter.length);
    if (typeof result !== 'string' || !result.length) {
      return null;
    }

    return result;
  };

  const removeWeaponAndSkinNameHash = (value: string | null): string | null => {
    if (!value) {
      return null;
    }

    const indx = value.indexOf(CSGOStashWeaponNameHashDelimiter);
    if (indx === -1) {
      return null;
    }

    const result = value.substring(0, indx);
    if (typeof result !== 'string' || !result.length) {
      return null;
    }

    return result;
  };

  const removeWeaponNamePostfix = (value: string | null): string | null => {
    if (!value) {
      return null;
    }

    const idx = value.indexOf(CSGOStashWeaponNamePostfix);
    if (idx === -1) {
      return value;
    }

    const result = value.substring(0, idx);
    if (typeof result !== 'string' || !result.length) {
      return null;
    }

    return result;
  };

  const splitWeaponAndSkinName = (value: string | null): [string, string] | [null, null] => {
    if (!value) {
      return [null, null];
    }

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < CSGOStashWeapons.length; ++i) {
      const weaponName = CSGOStashWeapons[i];
      const idx = value.indexOf(weaponName);
      if (idx === -1) {
        continue;
      }

      const name = value.substring(0, weaponName.length);
      const skin = value.substring(weaponName.length + 1);

      if (!name || !skin) {
        continue;
      }

      return [name, skin];
    }

    console.error(`Failed to find weapon type: "${value}"`);

    return [null, null];
  };

  return splitWeaponAndSkinName(
    removeWeaponNamePostfix(
      removeWeaponAndSkinNameHash(removeWeaponPrefixAndExtractFullWeaponAndSkinName(imageUrl)),
    ),
  );
};

export const parseCSGOStashSkin = (htmlData: string): CSGOStashSkin | null => {
  const $ = cheerio.load(htmlData);

  const url = parseOGURL($);
  const image3dUrl = parseOG3dImageURL($);
  const imageSideUrl = parseSideImageURL($);

  if (!url || !image3dUrl || !imageSideUrl) {
    return null;
  }

  const [name, skin] = parseWeaponAndSkinName(image3dUrl);
  if (!name || !skin) {
    return null;
  }

  return { name, baseUrl: url, image3dViewUrl: image3dUrl, imageSideViewUrl: imageSideUrl, skin };
};

export const parseCSGOStashCategories = (
  htmlData: string,
  categoryTypes?: CSGOStashCategoryType[],
): CSGOStashCategories | null => {
  const $ = cheerio.load(htmlData);
  return parseCategories($, categoryTypes);
};

export const parseCSGOStashCategorySkins = (htmlData: string): CSGOStashCategorySkins | null => {
  const $ = cheerio.load(htmlData);
  return parseCategorySkins($);
};

import { parseWeaponAndSkinName } from './csgostash.parser';

describe('CSGO-STASH.Parse', () => {
  it('should successful parse rifle and skin name', () => {
    const imageUrl =
      'https://steamcdn-a.akamaihd.net/apps/730/icons/econ/default_generated/weapon_ssg08_cu_ssg08_deathshead_light_large.78676bc5fce74af519abdccae4d119727c392faf.png';

    const result = parseWeaponAndSkinName(imageUrl);
    expect(result).not.toBeNull();
    expect(result).toHaveLength(2);
    expect(result[0]).toBe('ssg08');
    expect(result[1]).toBe('cu_ssg08_deathshead');
  });

  it('should successful parse knife and skin name', () => {
    const imageUrl =
      'https://steamcdn-a.akamaihd.net/apps/730/icons/econ/weapons/base_weapons/weapon_knife_outdoor.fdb3ce5ceef1584781759ef5a7bd6f819bf12e9b.png';

    const result = parseWeaponAndSkinName(imageUrl);
    expect(result).not.toBeNull();
    expect(result).toHaveLength(2);
    expect(result[0]).toBe('knife');
    expect(result[1]).toBe('outdoor');
  });

  it('should fail parsing an empty url', () => {
    const imageUrl = '';

    const result = parseWeaponAndSkinName(imageUrl);
    expect(result).not.toBeNull();
    expect(result).toHaveLength(2);
    expect(result[0]).toBeNull();
    expect(result[1]).toBeNull();
  });

  it('should fail parsing an partial url ', () => {
    const imageUrl =
      'https://steamcdn-a.akamaihd.net/apps/730/icons/econ/weapons/base_weapons/weapon_';

    const result = parseWeaponAndSkinName(imageUrl);
    expect(result).not.toBeNull();
    expect(result).toHaveLength(2);
    expect(result[0]).toBeNull();
    expect(result[1]).toBeNull();
  });

  it('should fail parsing an invalid url #1', () => {
    const imageUrl =
      'https://steamcdn-a.akamaihd.net/apps/730/icons/econ/weapons/base_weapons/weapon_knife.fdb3ce5ceef1584781759ef5a7bd6f819bf12e9b.png';

    const result = parseWeaponAndSkinName(imageUrl);
    expect(result).not.toBeNull();
    expect(result).toHaveLength(2);
    expect(result[0]).toBeNull();
    expect(result[1]).toBeNull();
  });

  it('should fail parsing an invalid url #2', () => {
    const imageUrl =
      'https://steamcdn-a.akamaihd.net/apps/730/icons/econ/weapons/base_weapons/weapon_outdoor.fdb3ce5ceef1584781759ef5a7bd6f819bf12e9b.png';

    const result = parseWeaponAndSkinName(imageUrl);
    expect(result).not.toBeNull();
    expect(result).toHaveLength(2);
    expect(result[0]).toBeNull();
    expect(result[1]).toBeNull();
  });
});

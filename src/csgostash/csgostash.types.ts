export enum CSGOStashCategoryType {
  Pistols,
  Rifles,
  SMGs,
  Heavy,
  Knives,
}

export const CSGOStashCategoryTypeSet = new Set(Object.keys(CSGOStashCategoryType));

export interface CSGOStashCategory {
  readonly category: CSGOStashCategoryType;
  readonly name: string;
  readonly url: string;
}

export type CSGOStashCategories = CSGOStashCategory[];

export interface CSGOStashCategorySkin {
  readonly name: string;
  readonly url: string;
}

export type CSGOStashCategorySkins = CSGOStashCategorySkin[];

export interface CSGOStashSkin {
  readonly name: string;
  readonly skin: string;
  readonly baseUrl: string;
  readonly image3dViewUrl: string;
  readonly imageSideViewUrl: string;
}

export const CSGOStashWeaponNamePrefix = 'weapon';

export const CSGOStashWeaponNamePostfix = '_light_large';

export const CSGOStashWeaponNameHashDelimiter = '.';
export const CSGOStashWeaponNameDelimiter = '_';

// SOURCE: https://totalcsgo.com/give-codes
export const CSGOStashWeapons = [
  // Pistols
  'deagle',
  'elite',
  'fiveseven',
  'glock',
  'cz75a',
  'hkp2000',
  'p250',
  'revolver',
  'tec9',
  'usp_silencer',

  // Shotguns
  'xm1014',
  'nova',
  'mag7',
  'sawedoff',

  // Machine Guns
  'm249',
  'negev',

  // Submachine Guns
  'mac10',
  'bizon',
  'mp5sd',
  'mp7',
  'mp9',
  'p90',
  'ump45',

  // Rifles
  'ak47',
  'aug',
  'famas',
  'galilar',
  'm4a1',
  'm4a1_silencer',
  'sg556',

  // Sniper Rifles
  'awp',
  'g3sg1',
  'scar20',
  'ssg08',

  // Grenades
  'tagrenade',
  'smokegrenade',
  'decoy',
  'hegrenade',
  'flashbang',
  'incgrenade',
  'molotov',
  'firebomb',

  // Bomb
  'c4',

  // Taser
  'taser',

  // Health
  'healthshot',

  // Knifes
  'knife_css',
  'knife_flip',
  'knife_gut',
  'knife_karambit',
  'knife_m9_bayonet',
  'knife_tactical',
  'knife_falchion',
  'knife_survival_bowie',
  'knife_butterfly',
  'knife_push',
  'knife_cord',
  'knife_canis',
  'knife_ursus',
  'knife_gypsy_jackknife',
  'knife_outdoor',
  'knife_stiletto',
  'knife_widowmaker',
  'knife_skeleton',
  'knife_default_ct',
  'knife_t',
  'knife',
  'knifegg',
  'bayonet',
];

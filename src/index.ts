import * as path from 'path';

import CSGOStashService from './csgostash/csgostash.service';
import { CSGOStashSkin, CSGOStashCategoryType } from './csgostash';
import { sleep, randomNumber } from './utilities';

const delayRequest = (min: number, max: number) => () => {
  const delayMs = randomNumber(min, max);
  console.log(`Delaying request for: ${delayMs}ms`);
  return sleep(delayMs);
};

const buildSkinAssetPath = (skin: CSGOStashSkin, postfix: string) => {
  return path.resolve(process.cwd(), 'assets', `${skin.name}__${skin.skin}__${postfix}.png`);
};

const main = async () => {
  try {
    console.log('https://csgostash.com Scrapper (c) Jan (impmja) Schulte');

    // NOTE: Poor mans break
    let shouldShutdown = false;
    process.on('SIGINT', () => {
      shouldShutdown = true;
      console.log('Shutdown');
      return process.exit(1);
    });

    const DELAY_MIN_BETWEEN_REQUESTS = 100;
    const DELAY_MAX_BETWEEN_REQUESTS = 500;
    const sleepFn = delayRequest(DELAY_MIN_BETWEEN_REQUESTS, DELAY_MAX_BETWEEN_REQUESTS);

    const csgoStash = new CSGOStashService();

    // NOTE: If you want to parse a specific category/categories
    // pass a list of CSGOStashCategoryType's to the function call like:
    // csgoStash.fetchCategories([CSGOStashCategoryType.Knives, ...]);
    const categories = await csgoStash.fetchCategories([
      CSGOStashCategoryType.Pistols,
      CSGOStashCategoryType.Rifles,
      CSGOStashCategoryType.SMGs,
      CSGOStashCategoryType.Heavy,
      CSGOStashCategoryType.Knives,
    ]);
    if (!categories || !categories.length) {
      console.error('Failed to fetch categories');
      return process.exit(0);
    }
    console.log(`Found categories: "${categories.length}"`);

    let downloadedSkins = 0;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < categories.length; ++i) {
      try {
        if (shouldShutdown) {
          break;
        }

        await sleepFn();

        const category = categories[i];
        console.log(`Fetching category: "${category.name}"`);
        const categorySkins = await csgoStash.fetchCategorySkins(category);
        if (!categorySkins || !categorySkins.length) {
          continue;
        }
        console.log(`Found skin categories: "${categorySkins.length}"`);

        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < categorySkins.length; ++j) {
          try {
            if (shouldShutdown) {
              break;
            }

            await sleepFn();

            const categorySkin = categorySkins[j];
            console.log(`Fetching skin category: "${categorySkin.name}"`);
            const skin = await csgoStash.fetchCategorySkin(categorySkin);

            if (skin) {
              console.log('Skin found:', `${skin.name} ${skin.skin}`);

              console.log('Downloading', skin.image3dViewUrl);
              await csgoStash.fetchAndSaveSkin(skin.image3dViewUrl, buildSkinAssetPath(skin, '3d'));
              downloadedSkins++;

              console.log('Downloading', skin.imageSideViewUrl);
              await csgoStash.fetchAndSaveSkin(
                skin.imageSideViewUrl,
                buildSkinAssetPath(skin, '2d'),
              );
              downloadedSkins++;
            }
          } catch (e) {
            console.error(e);
          }
        }
      } catch (e) {
        console.error(e);
      }

      console.log(`Done scrapping!`);
      console.log(`Total skins downloaded: "${downloadedSkins}"`);
    }

    return void 0;
  } catch (e) {
    return console.error(e);
  }
};

main();

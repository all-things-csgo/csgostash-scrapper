
# Build
FROM node:10.15.0-alpine as node_build

WORKDIR /app

COPY package.json yarn.lock tsconfig.settings.json tsconfig.json tsconfig.prod.json /app/
COPY src/ /app/src/

RUN yarn install --frozen-lockfile && \
  yarn run build:prod && \
  yarn install --frozen-lockfile --prod && \
  yarn cache clean && \
  # Remove everything with "*.ts" in its name -> all Typescript related files we dont want anymore!
  rm -rf $(ls /app/dist/*.ts*)

# Deploy
FROM node:10.15.0-alpine

COPY --from=node_build /app/node_modules /app/node_modules
COPY --from=node_build /app/dist/ /app

WORKDIR /app

ENV NODE_ENV production

ENTRYPOINT ["node", "/app/index.js"]
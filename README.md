# csgostash scrapper

> This is a [NodeJS](https://nodejs.org/) and [cheerio](https://github.com/cheeriojs/cheerio) based web-scrapper for the [CSG-Stash](https://csgostash.com) website to acquire all possible weapon skins. It uses some very simple "disguise" mechanism to prevent ip-blocking such as random User-Agents and random timeouts between requests.
>
> **Disclaimer**: The use of this application **is at your own risk**. You are fully liable for any damage you might cause. This application scraps and downloads images from [CSG-Stash](https://csgostash.com), so don't spam them nor distribute their images in any way. You have been warned. This application was made for research purposes only!

## Developing

### Prerequisites

- NodeJS ver. 10.15.0+ (Use nvm!)
- [Node Version Manager](https://github.com/creationix/nvm)
- [Docker-CE](https://docs.docker.com/install/) & [Docker-Compose](https://docs.docker.com/compose/install/)

### Installing / Getting started

```bash
# Activate NodeJS Version defined within the .nvmrc file
$ nvm use

# or

$ nvm use 10.15.0
```

```bash
# Install default dependencies
$ yarn install
```

### Debugging

>This project contains a VSCode launch configuration which allows to execute the Typescript files directly via the Debugger. Just put a Breakpoint where you want and press F5.

### Building

#### Local

```bash
# Build application in development mode
$ yarn build:dev
```

```bash
# Build application in production mode
$ yarn build:prod
```

#### Docker

```bash
# Build the Docker image & start the container / application
$ make up
```

```bash
# Rebuild the Docker image & start the container / application from ground up
$ make rebuild
```

### Linting & Testing

```bash
# Lint the applications source
$ yarn lint
```

```bash
# Run all test solution(s)
$ yarn test
```

```bash
# Continuously run all test solution(s)
$ yarn test:watch
```

### Executing

#### Development

```bash
# Execute the application via ts-node
$ yarn start:dev
```

#### Production

```bash
# Execute the application via node
$ yarn start:prod
```

#### Docker

```bash
# Starts the Docker container
$ make start
```

```bash
# Stops the Docker container
$ make stop
```

### Shell & Logs

#### Docker

```bash
# Shows the stdout of the Docker container
$ make logs
```

```bash
# Connects to the Docker containers bash shell
$ make bash

# or

$ make shell

# You leave the bash shell with
$ exit
```

### Licenses

> [MIT](./LICENSE)

## Author

>(c) Jan Schulte
>
>Date: 2020-01-19 03:02:58
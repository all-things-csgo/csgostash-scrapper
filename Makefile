# Docker machine states
ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

up:
	docker-compose up -d

down:
	docker-compose down

start:
	docker-compose start

stop:
	docker-compose stop

restart: stop start

state:
	docker-compose ps

# General
build:
	docker-compose build

rebuild:
	docker-compose stop
	docker-compose rm --force what-is-life-server
	docker-compose build --no-cache --pull
	docker-compose up -d --force-recreate

bash: shell

shell:
	docker-compose exec what-is-life-server sh

logs:
	docker logs what-is-life-server

# Argument fix workaround
%:
	@:
